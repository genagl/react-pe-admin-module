import { Controller, UseGuards } from "@nestjs/common";
import { ApiBearerAuth, ApiTags } from "@nestjs/swagger";
import { PEController } from "src/common/pe.controller";
import { CategoryService } from "./category.service";
import { JwtAuthGuard } from "@auth/guards/jwt.guard";

@Controller('categories') 
@ApiTags("categories")
export class CategoryController extends PEController {
    constructor( private readonly _categoryService: CategoryService ){
        super( _categoryService );
    }
}