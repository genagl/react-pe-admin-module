import { PEResolver } from "@common/pe.resolver";
import { LandService } from "@lands/land.service";
import { Resolver } from "@nestjs/graphql";
import { UsersService } from "@users/users.service";
import { CreatePostDto } from "./dto/create-post.dto";
import { PostEntity } from "./entities/post.entity";
import { PostService } from "./post.service";


@Resolver(() => PostEntity)
export class PostResolver extends PEResolver(
    PostEntity,
    CreatePostDto, 
    "Post"
) {
    constructor( 
        private readonly _servicer: PostService,
        private readonly _lands: LandService,
        private readonly _users: UsersService,
    ) {
        super( _servicer, _lands, _users ); 
    }
}