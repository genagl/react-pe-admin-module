import { PEResolver } from "@common/pe.resolver";
import { Resolver } from "@nestjs/graphql";
import { TagEntity } from "./entities/tag.entity";
import { TagService } from "./tag.service"; 
import { CreateTagDto } from "./dto/create-tag.dto";
import { LandService } from "@lands/land.service";
import { UsersService } from "@users/users.service";


@Resolver(() => TagEntity)
export class TagResolver extends PEResolver(TagEntity, CreateTagDto, "Tag") {
    constructor( 
        private readonly _servicer: TagService,
        private readonly _lands: LandService,
        private readonly _users: UsersService, 
    ){
        super( _servicer, _lands, _users );
    }
}