import { Injectable } from "@nestjs/common";  
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm"; 
import { PERepository } from "@common/repositories";
import { TagEntity } from "./entities/tag.entity";
import { CreateTagDto } from "./dto/create-tag.dto";

@Injectable()
export class TagService extends PERepository<TagEntity, CreateTagDto> {
    constructor( 
        @InjectRepository(TagEntity)
        private readonly _tagRepository: Repository<TagEntity>,
    ) {
        super(_tagRepository, "tag") 
    }
}