import { PEResolver } from "@common/pe.resolver";
import { Resolver } from "@nestjs/graphql";
import { CategoryService } from "./category.service";
import { CategoryEntity } from "./entities/category.entity"; 
import { CreateCategoryDto } from "./dto/create-category.dto";
import { LandService } from "@lands/land.service";
import { UsersService } from "@users/users.service";


@Resolver(() => CategoryEntity)
export class CategoryResolver extends PEResolver(CategoryEntity, CreateCategoryDto, "Category") {
    constructor( 
        private readonly _servicer: CategoryService,
        private readonly _lands: LandService,
        private readonly _users: UsersService, 
    
    ){
        super( _servicer, _lands, _users );
    }
}