import { Field, ObjectType } from '@nestjs/graphql';
import { PELandedEntity } from "@src/common/entities";
import { Entity, JoinTable, ManyToMany } from "typeorm";
import { PostEntity } from "./post.entity";

@Entity("tags")
@ObjectType("Tag", { description: 'tags' })
export class TagEntity extends PELandedEntity {
    @ManyToMany(() => PostEntity, post => post.category )
    @JoinTable()
    @Field({description: "Children Resources", nullable: 'items' })
    posts?: PostEntity[]; 
}