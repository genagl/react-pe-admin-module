import { PELandedEntity } from "@common/entities";
import { Field, ObjectType } from '@nestjs/graphql';
import { Entity, JoinTable, ManyToMany } from "typeorm";
import { CategoryEntity } from "./category.entity";
import { TagEntity } from "./tag.entity";

@Entity("posts") 
@ObjectType("Post", { description: "Simple post data type" })
export class PostEntity extends PELandedEntity {
    @ManyToMany(() => CategoryEntity, category => category.posts)
    @JoinTable()
    @Field({  description: 'Categories, that this Resource added', nullable: 'items' })
    category?: CategoryEntity[]

    @ManyToMany(() => TagEntity, tag => tag.posts)
    @JoinTable()
    @Field({  description: 'Tags, that this Resource added', nullable: 'items' })
    tag?: TagEntity[]
}