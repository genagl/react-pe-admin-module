import { Field, ObjectType } from '@nestjs/graphql';
import { PELandedEntity } from "@common/entities";
import { Entity, JoinTable, ManyToMany } from "typeorm";
import { PostEntity } from "./post.entity";
import { IPe } from '@common/interface';

@Entity("categories")
@ObjectType("Category", { description: 'categories' })
export class CategoryEntity extends PELandedEntity implements IPe {
    @ManyToMany(() => PostEntity, post => post.category )
    @JoinTable()
    @Field({description: "Children Resources", nullable: 'items' })
    posts?: PostEntity[];
}