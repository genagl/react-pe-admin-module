import { Controller, UseGuards } from "@nestjs/common";
import { ApiBearerAuth, ApiTags } from "@nestjs/swagger";
import { PEController } from "src/common/pe.controller";
import { TagService } from "./tag.service";
import { JwtAuthGuard } from "@auth/guards/jwt.guard";

@Controller('tags') 
@ApiTags("tags") 
export class TagController extends PEController {
    constructor( private readonly _tagService: TagService ){
        super( _tagService );
    }        
}