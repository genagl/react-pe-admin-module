import { InputType } from "@nestjs/graphql";
import { CreatePEDto } from "src/common/dto/create-pe.dto";

@InputType("TagInput")
export class CreateTagDto extends CreatePEDto {
    
}