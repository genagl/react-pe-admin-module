import { InputType } from "@nestjs/graphql";
import { CreatePEDto } from "@common/dto/create-pe.dto";

@InputType("PostInput")
export class CreatePostDto extends CreatePEDto {
    
}