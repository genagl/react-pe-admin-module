import { PartialType } from '@nestjs/swagger';
import { CreateTagDto } from './create-tag.dto';
import { InputType } from '@nestjs/graphql';

@InputType()
export class UpdateTagDto extends PartialType(CreateTagDto) {

}