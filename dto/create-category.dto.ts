import { InputType } from "@nestjs/graphql";
import { CreatePEDto } from "src/common/dto/create-pe.dto";

@InputType("CategoryInput")
export class CreateCategoryDto extends CreatePEDto { }