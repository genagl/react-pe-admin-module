import { Injectable } from "@nestjs/common";  
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm"; 
import { PERepository } from "@common/repositories";
import { CategoryEntity } from "./entities/category.entity";
import { CreateCategoryDto } from "./dto/create-category.dto";

@Injectable()
export class CategoryService extends PERepository<CategoryEntity, CreateCategoryDto> {
    constructor( 
        @InjectRepository(CategoryEntity)
        private readonly _categoryRepository: Repository<CategoryEntity>,
    ) {
        super(_categoryRepository, "category") 
    }
}