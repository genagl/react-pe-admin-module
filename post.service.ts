import { Injectable } from "@nestjs/common"; 
import { PostEntity } from "./entities/post.entity";
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm"; 
import { PERepository } from "@common/repositories";
import { ID } from "@common/scalars/ID.scalar";
import { CategoryEntity } from "./entities/category.entity";
import { CreatePostDto } from "./dto/create-post.dto";

@Injectable()
export class PostService extends PERepository<PostEntity, CreatePostDto> {
    constructor( 
        @InjectRepository(PostEntity)
        private readonly _postRepository: Repository<PostEntity>,
        @InjectRepository(CategoryEntity)
        private readonly _categoryRepository: Repository<CategoryEntity>,

    ) {
        super(_postRepository, "post") 
    }

    async addCategories (postId: ID, categoryIds: ID[]) : Promise<any> {
        const post = await this.findById(postId);
        if(!post) {
            throw new Error("Resource not exists.");
        }
        const categories = await this._categoryRepository.findByIds( categoryIds );
        if(!categories.length) {
            throw new Error( "Terms not exists")
        }
        post.category = categories;
        return this._postRepository.save(post);
    }
}